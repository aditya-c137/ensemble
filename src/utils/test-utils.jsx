import React from 'react'
import { render } from '@testing-library/react'
import _ from 'lodash'

expect.extend({
  toHaveClass(received, className) {
    let stripSuffix = (string)=> {
      if((string.match(/\-/g) || []).length>1) {
        return string.slice(0,className.lastIndexOf('-'))
      }
      return string
    }

    let stripped = stripSuffix(className)
    return _.some(received.classList, item=>item.startsWith(stripped)) ?
    {
      message: () => `Element has class - ${className}`,
      pass: true,
    } : {
      message: () => `Element did not have class - ${className}. 
                      Had ${JSON.stringify(received.classList)}`,
      pass: false,
    }
  }
});



export function getClasses(element) {
  const { useStyles } = element.type;

  let classes;
  function Listener() {
    classes = useStyles(element.props);
    return null;
  }
  render(<Listener />);

  return classes;
}
