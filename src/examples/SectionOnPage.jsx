import React from 'react'
import ReactMarkdown from 'react-markdown'

export default React.forwardRef(function(props, ref) {
    const input = `# ${props.heading}\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis efficitur, augue sed rutrum dignissim, 
    sem diam ornare orci, non pellentesque neque est vitae tortor. Aenean sed quam orci. In sed sapien 
    ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ligula nunc, accumsan pretium 
    leo quis, sodales vehicula urna. Phasellus rutrum libero id risus dapibus suscipit. Duis porttitor 
    risus sed egestas aliquam. Pellentesque a urna et nibh vehicula elementum. Curabitur quis dapibus 
    ante. Mauris pretium augue quis libero malesuada, quis mattis orci commodo. Nam in posuere lacus, quis 
    tincidunt ipsum. Aenean odio metus, sollicitudin vel tempus a, finibus lacinia.`

    
    return (<section id={props.id} ref={ref}>
                <ReactMarkdown source={input} />
            </section>)
})

