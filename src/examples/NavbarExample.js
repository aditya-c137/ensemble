import React, { useEffect, useState } from 'react';
import { AppBar, Box, useMediaQuery, 
         createStyles, makeStyles , Grid } from '@material-ui/core'
import {AccountCircle} from '@material-ui/icons'
import Navbar from '../components/Navbar/Navbar';
import NavbarChild from '../components/NavbarChild';
import SectionOnPage from './SectionOnPage';
import _ from 'lodash'

let baseConfigs = [
  {
    heading : "About Us",
    id : "AboutUs"
  },
  {
    heading : "Bodhi",
    id : "Bodhi"
  },
  {
    heading : "Akupara",
    id : "Akupara"
  },
  {
    heading : "Blue Trumpets",
    id : "BlueTrumpets"
  },
  {
    heading : "Contact Us",
    id : "ContactUs"
  }
]

let useStyles = makeStyles(theme=> createStyles({
  container : {
    width : "100%",
    display : "flex",
    flexDirection : "row",
    flexWrap:"nowrap"
  },
  main : {
    // marginTop : 60,
  },
  sidebar : {
    position: "fixed",
    top : 0,
    right : 0,
    width : "25vw",
    height : "100vh",
    display : "flex",
    justifyContent: "center"

  },
  appBar : {
    height : props => props.appBarHeight
  },
}))

let useNavbarStyles = makeStyles(theme=>createStyles({
  navBar : {
    height : props => props.appBarHeight-10
  },
}))

let useChildStyle = makeStyles(theme=>createStyles({
  /* Styles applied to the root element. */
  root: {
    ...theme.typography.button,
    maxWidth: 264,
    minWidth: 72,
    position: 'relative',
    boxSizing: 'border-box',
    minHeight: 48,
    flexShrink: 0,
    padding: '6px 12px',
    [theme.breakpoints.up('sm')]: {
      padding: '6px 24px',
    },
    overflow: 'hidden',
    whiteSpace: 'normal',
    textAlign: 'center',
  }
}))




let UpdateableNavbar = (props)=> {
  let {offsetToTarget = 0, orientation, appBarHeight = 70,
       configs, mainRef} = props
  let [activeRef, setActiveRef] = useState(null)
  
  let navbarClasses = useNavbarStyles({appBarHeight})
  let childClasses = useChildStyle()

  let spy =()=>{
    let targetRefs=
    _.sortBy(
      _.filter(_.map(configs, config => config.targetRef), 
               targetRef=> {
                  let bound = targetRef.current.getBoundingClientRect()
                  return bound.bottom >= 0 || bound.top <= window.innerHeight;
                }),
      targetRef => targetRef.current.getBoundingClientRect().top)
    if(targetRefs && targetRefs[0] && activeRef !== targetRefs[0]) {
      setActiveRef(targetRefs[0])
    }
  }
  let throttledSpy = _.throttle(spy, 66)

  useEffect(()=>{
    let main = mainRef.current
    main.addEventListener('scroll', throttledSpy)
    return ()=>main.removeEventListener('scroll', throttledSpy)
  })

  return <Navbar scrollButtons="on" variant='scrollable' 
            value={activeRef && activeRef.current.value}
            offsetToTarget={offsetToTarget}
            orientation={orientation} className={navbarClasses.navbar}>
              { _.map(configs, (config)=>(
                    <NavbarChild key={config.id} 
                                  value={config.id} 
                                  targetRef={config.targetRef} 
                                  label={config.heading}
                                  icon={config.icon}
                                  classes={childClasses} />))}
          </Navbar>
}


let MainSection=React.forwardRef(function MainSection (props, ref) {
  let {configs} = props
  return <main ref={ref}>
            { _.map(configs, (config)=><SectionOnPage key={config.id} 
                                                      id={config.id} 
                                                      heading={config.heading} 
                                                      ref={config.targetRef} />)}
          </main>
})

let RightLayout =(props)=> {
  const {configs, appBarHeight = 70} = props
  let classes = useStyles({appBarHeight})

  const mainRef=React.createRef()
  

  return <React.Fragment>
            <Grid container direction="row">
              <Grid item xs={9}>
                <MainSection configs={configs} ref={mainRef} />
              </Grid>
              <Grid item xs={3} />
          </Grid>
          <div className={classes.sidebar}>
            <UpdateableNavbar orientation="vertical" configs={configs} mainRef={mainRef} />
          </div>
        </React.Fragment> 
}

let TopLayout=(props)=>{
  const {configs, appBarHeight = 70} = props
  const classes = useStyles({appBarHeight})
  const mainRef=React.createRef()

  return <Box >
            <AppBar  position="sticky" className={classes.appBar}>
              <UpdateableNavbar orientation="horizontal" configs={configs} mainRef={mainRef} />
            </AppBar>
            <MainSection configs={configs} ref={mainRef} />
          </Box>
}




function NavbarExample() {
  const matches = useMediaQuery('(min-width:600px)');
  let classes = makeStyles({
    navBarIcon : {
      ['@media (min-width:600px)'] : {
        width: "25%"
      },
      display : "inline-block",
      width: "15%",
      minWidth: 48,
      color: "#444"
    }
  })()
  let navbarIcon = <AccountCircle className={classes.navBarIcon} />
  let configs = _.map(baseConfigs, config=>{
                    return { 
                      ...config, 
                      icon : config.icon || navbarIcon, 
                      targetRef : React.createRef()
                    }
                  })
                        
  return matches ? <RightLayout configs={configs} /> : <TopLayout configs={configs} /> 
}

export default NavbarExample;
