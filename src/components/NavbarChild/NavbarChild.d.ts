import React, { Ref } from 'react';
import { ExtendButtonBase, ExtendButtonBaseTypeMap } from '@material-ui/core/ButtonBase';
import { OverrideProps } from '@material-ui/core/OverridableComponent';

export type NavbarChildTypeMap<P = {}, D extends React.ElementType = HTMLDivElement,T extends React.ElementType = HTMLDivElement> = ExtendButtonBaseTypeMap<{
  props: P & {
    /**
     * This prop isn't supported.
     * Use the `component` prop if you need to change the children structure.
     */
    children?: null;
    /**
     * If `true`, the NavbarChild will be disabled.
     */
    disabled?: boolean;
    /**
     * If `true`, the  keyboard focus ripple will be disabled.
     */
    disableFocusRipple?: boolean;
    /**
     * The icon element.
     */
    icon?: string | React.ReactElement;
    /**
     * The label element.
     */
    label?: React.ReactNode;
    /**
     * You can provide your own value. Otherwise, we fallback to the child position index.
     */
    value?: any;
    /**
     * NavbarChild labels appear in a single row.
     * They can use a second line if needed.
     */
    wrapped?: boolean;
    /*
    * Ref of the connected element.
    */
   targetRef: Ref<T>
  };
  defaultComponent: D;
  classKey: NavbarChildClassKey;
}>;

/**
 *
 * Demos:
 *
 * - [NavbarChild]()
 *
 * API:
 *
 * - [NavbarChild API]()
 * - inherits [ButtonBase API](https://material-ui.com/api/button-base/)
 */
declare const NavbarChild: ExtendButtonBase<NavbarChildTypeMap>;

export type NavbarChildClassKey =
  | 'root'
  | 'labelIcon'
  | 'textColorInherit'
  | 'textColorPrimary'
  | 'textColorSecondary'
  | 'selected'
  | 'disabled'
  | 'fullWidth'
  | 'wrapped'
  | 'wrapper';

export type NavbarChildProps<
  D extends React.ElementType = NavbarChildTypeMap['defaultComponent'],
  P = {}
> = OverrideProps<NavbarChildTypeMap<P, D>, D>;

export default NavbarChild;
