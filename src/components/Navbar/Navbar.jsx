import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import _ from 'lodash'
import { getNormalizedScrollLeft, detectScrollType } from 'normalize-scroll-left';
import ScrollbarSize from 'react-scrollbar-size';
import NavbarIndicator from './NavbarIndicator';
import NavbarScrollButton from '../NavbarScrollButton';
import {useTheme, withStyles, ownerWindow, useEventCallback} from '@material-ui/core';
import { useEffect } from 'react';
import 'element-scroll-polyfill';

export const styles = (theme) => ({
  /* Styles applied to the root element. */
  root: {
    overflow: 'hidden',
    minHeight: 48,
    WebkitOverflowScrolling: 'touch', // Add iOS momentum scrolling.
    display: 'flex',
  },
  /* Styles applied to the root element if `orientation="vertical"`. */
  vertical: {
    flexDirection: 'column',
  },
  /* Styles applied to the flex container element. */
  flexContainer: {
    display: 'flex',
  },
  /* Styles applied to the flex container element if `orientation="vertical"`. */
  flexContainerVertical: {
    flexDirection: 'column',
  },
  /* Styles applied to the flex container element if `centered={true}` & `!variant="scrollable"`. */
  centered: {
    justifyContent: 'center',
  },
  /* Styles applied to the navbarList element. */
  scroller: {
    position: 'relative',
    display: 'inline-block',
    flex: '1 1 auto',
    whiteSpace: 'nowrap',
  },
  /* Styles applied to the navbarList element if `!variant="scrollable"`. */
  fixed: {
    overflowX: 'hidden',
    width: '100%',
  },
  /* Styles applied to the navbarList element if `variant="scrollable"`. */
  scrollable: {
    overflowX: 'scroll',
    // Hide dimensionless scrollbar on MacOS
    scrollbarWidth: 'none', // Firefox
    '&::-webkit-scrollbar': {
      display: 'none', // Safari + Chrome
    },
  },
  /* Styles applied to the `ScrollButtonComponent` component. */
  scrollButtons: {},
  /* Styles applied to the `ScrollButtonComponent` component if `scrollButtons="auto"` or scrollButtons="desktop"`. */
  scrollButtonsDesktop: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  /* Styles applied to the `TabIndicator` component. */
  indicator: {},
});

// const prependHashIfRequired = (string)=>`${string.match(/^#/i)?'':'#'}${string}`
// const stripPrefixHashIfRequired = (string)=>`${string.match(/^#/i)?string.slice(1):string}`


const Navbar = React.forwardRef(function Navbar(props, ref) {
  const {
    'aria-label': ariaLabel,
    'aria-labelledby': ariaLabelledBy,
    // action,
    isCentered = false,
    children,
    classes,
    className,
    component: Component = 'nav',
    indicatorColor = 'secondary',
    orientation = 'horizontal',
    ScrollButtonComponent = NavbarScrollButton,
    scrollButtons = 'auto',
    NavbarIndicatorProps = {},
    NavbarScrollButtonProps,
    textColor = 'inherit',
    value,
    onChange,
    offsetToTarget,
    variant = 'standard',
    ...other
  } = props;

  const navbarRef = React.useRef(null); //is this required?
  const navbarListRef = React.useRef(null);

  // Scrolling related

  const [state, setState] = React.useState({
    value,
    triggerNavigation : false
  });

  let valueToIndex = new Map();
  React.Children.forEach(children, (child, index) => {
    if (!React.isValidElement(child)) {
      return null;
    }
    const childValue = child.props.value === undefined ? index : child.props.value;
    valueToIndex.set(childValue, index);
  })
  
 
  useEffect(()=> {
    if(state.triggerNavigation) {
      let index = valueToIndex.get(state.value)
      if(_.isNil(index))  {
        console.error(
          `Material-UI: The value set in Navbar is invalid.None of the Navbar's children match 
          with \`${state.value}\`. It must match one of the following values - ${_.join(valueToIndex.keys(), ', ')}`);
      } else {
        let child = children[index]
        if(child && child.props.targetRef.current) {
          let offset = child.props.targetRef.current.offsetTop + (offsetToTarget || 0)
          window.scrollTo(0, offset);
        }
      } 
    }
  })

  // useEffect(()=> {
  //   let onLoad=()=>{
  //     // Causes redraw. Optimise.
  //     if(window.location.hash !== "undefined" && valueToIndex.has(window.location.hash)) {
  //       setState({
  //         value : stripPrefixHashIfRequired(window.location.hash),
  //         triggerNavigation : true
  //       })
  //     }
  //   }
  //   window.addEventListener('load', onLoad);
  //   return ()=> window.removeEventListener('load', onLoad)
  // })
  
    
  const linkClicked=(event, value)=>{
    let shouldPreventDefault = onChange && onChange(event, value)
    if(!shouldPreventDefault) {
      setState({
        value,
        triggerNavigation : true
      })
    }
  }
  
  
  return (
    <Component className={clsx(classes.root,
                              {
                                [classes.vertical]: orientation === 'vertical',
                              },
                              className,
                            )}
                ref={ref}
                {...other}>
        <NavbarListFragmentWithScrollElements
            ariaLabel={ariaLabel}
            ariaLabelledBy={ariaLabelledBy}
            // action={action}
            isCentered={isCentered}
            classes={classes}
            className={className}
            indicatorColor={indicatorColor}
            orientation={orientation}
            ScrollButtonComponent={ScrollButtonComponent}
            scrollButtons = {scrollButtons}
            NavbarIndicatorProps = {NavbarIndicatorProps}
            NavbarScrollButtonProps={NavbarScrollButtonProps}
            textColor = {textColor}
            value={state.value}
            onChange={linkClicked}
            variant={variant}
            ref={{navbarRef, navbarListRef}}
            {...other}> 
          {children}
        </NavbarListFragmentWithScrollElements>
    </Component>
  );
});

// TODO implement scroll.
const scroll = (scrollStart, ref, scrollValue) => { 
  let attrib = _.lowerCase(_.startsWith(scrollStart,'scroll')?scrollStart.slice(6):scrollStart)
  ref.current.scrollTo({[attrib] : scrollValue, behavior: 'smooth'})
}

// const scroll = (scrollValue) => {
//   animate(scrollStart, tabsRef.current, scrollValue);
// };

const scrollNavbar = (navbarRef, vertical, scrollStart, isRtl, delta) => {
  let scrollValue = navbarRef.current[scrollStart];
  if (vertical) {
    scrollValue += delta;
  } else {
    scrollValue += delta * (isRtl ? -1 : 1);
    // Fix for Edge
    scrollValue *= isRtl && detectScrollType() === 'reverse' ? -1 : 1;
  }
  scroll(scrollStart, navbarRef, scrollValue);
};


const NavbarListFragmentWithScrollElements =  React.forwardRef((props, ref) => {
  let {
    ariaLabel,
    ariaLabelledBy,
    // action,
    value,
    onChange,
    orientation,
    variant,
    isCentered,
    classes,
    textColor,
    indicatorColor,
    NavbarIndicatorProps,
    children: childrenProp,
    ScrollButtonComponent,
    NavbarScrollButtonProps,
    scrollButtons,
    ...other
  } = props
  const {navbarRef, navbarListRef}=ref
  if (process.env.NODE_ENV !== 'production') {
    if (isCentered && variant === 'isScrollable') {
      console.error(
        'Material-UI: You can not use the `isCentered={true}` and `variant="scrollable"` properties ' +
          'at the same time on a `Navbar` component.',
      );
    }
  }

  const theme = useTheme()

  const scrollable = variant === 'scrollable';
  const isRtl = theme.direction === 'rtl';
  const vertical = orientation === 'vertical';

  const scrollStart = vertical ? 'scrollTop' : 'scrollLeft';
  const start = vertical ? 'top' : 'left';
  const end = vertical ? 'bottom' : 'right';
  const clientSize = vertical ? 'clientHeight' : 'clientWidth';
  const size = vertical ? 'height' : 'width';


  // There should be easier way for this.
  const [mounted, setMounted] = React.useState(false);

  React.useEffect(() => {
    setMounted(true);
  }, []);

  // Create children from the provided element
  const valueToIndex = new Map();
  const children = React.Children.map(childrenProp, (child, index) => {
    if (!React.isValidElement(child)) {
      return null;
    }
    const childValue = child.props.value === undefined ? index : child.props.value;
    valueToIndex.set(childValue, index);
    const selected = childValue === value;
    return React.cloneElement(child, {
      fullWidth: variant === 'fullWidth',
      selected,
      onChange,
      textColor,
      value: childValue,
    })
  })


  // Start indicator related logic.


  // The indicator style will use absolute positioning to show the selected 
  // entry.
  const [indicatorStyle, setIndicatorStyle] = React.useState({});

  const updateIndicatorState = useEventCallback(() => {
    let newIndicatorStyle = 
      computeIndicatorStyle(navbarRef, navbarListRef, vertical, theme.direction, value, valueToIndex)
    // IE 11 support, replace with Number.isNaN
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(indicatorStyle[start]) || isNaN(indicatorStyle[size])) {
      setIndicatorStyle(newIndicatorStyle);
    } else {
      // Can this not be a deep compare?
      const dStart = Math.abs(indicatorStyle[start] - newIndicatorStyle[start]);
      const dSize = Math.abs(indicatorStyle[size] - newIndicatorStyle[size]);

      if (dStart >= 1 || dSize >= 1) {
        setIndicatorStyle(newIndicatorStyle);
      }
    }
  });

  // The indicator component
  let styleFromProps = NavbarIndicatorProps.style || []
  const indicator = (
    <NavbarIndicator
      className={classes.indicator}
      orientation={orientation}
      color={indicatorColor}
      {...NavbarIndicatorProps}
      style={{
        ...indicatorStyle,
        ...styleFromProps
      }}
    />
  );

  // End indicator related logic.

  


  // Should scroll buttons be shown or not.
  const [displayScroll, setDisplayScroll] = React.useState({
    start: false,
    end: false,
  });

  const [scrollerStyle, setScrollerStyle] = React.useState({
    overflow: 'hidden',
    marginBottom: null,
  });

  const scrollButtonsActive = displayScroll.start || displayScroll.end;
  const showScrollButtons =
    ((scrollButtons === 'auto' && scrollButtonsActive) ||
      scrollButtons === 'desktop' ||
      scrollButtons === 'on');

  // Handlers for scroll button
  const handleStartScrollClick = () => {
    scrollNavbar(navbarRef, vertical, scrollStart, isRtl, -navbarRef.current[clientSize]);
  };

  const handleEndScrollClick = () => {
    scrollNavbar(navbarRef, vertical, scrollStart, isRtl, navbarRef.current[clientSize]);
  };

  // Handle change in scroll bar size.
  const handleScrollbarSizeChange = React.useCallback((scrollbarSize) => {
    
    // setScrollerStyle({
    //   overflow: null,
    //   marginBottom: -scrollbarSize.height,
    // });
  }, []);
  
  // Should the scroll buttons be displayed or not depends on the current scroll location
  const updateScrollButtonState = useEventCallback(() => {
   
    if (scrollable && scrollButtons !== 'off') {
      const { scrollTop, scrollHeight, clientHeight, scrollWidth, clientWidth } = navbarRef.current;
      let showStartScroll;
      let showEndScroll;
      if (vertical) {
        showStartScroll = scrollTop > 1;
        showEndScroll = scrollTop < scrollHeight - clientHeight - 1;
      } else {
        const scrollLeft = getNormalizedScrollLeft(navbarRef.current, theme.direction);
        // use 1 for the potential rounding error with browser zooms.
        showStartScroll = isRtl ? scrollLeft < scrollWidth - clientWidth - 1 : scrollLeft > 1;
        showEndScroll = !isRtl ? scrollLeft < scrollWidth - clientWidth - 1 : scrollLeft > 1;
      }
      if (showStartScroll !== displayScroll.start || showEndScroll !== displayScroll.end) {
        setDisplayScroll({ start: showStartScroll, end: showEndScroll });
      }
    }
  });


  // On scroll, update the button state.
  // As it is an expensive operation, do it sparingly.
  const handleNavbarScroll = React.useMemo(
    () => _.debounce(() => { updateScrollButtonState() }),
    [updateScrollButtonState],
  );

  // Cancel invocation immediately after mount.
  React.useEffect(() => {
    return () => {
      handleNavbarScroll.cancel();
    };
  }, [handleNavbarScroll]);

  // If a button that is partially in view is selected, scroll so that 
  // it is completely in the view.
  // Needs complete rewrite
  const scrollSelectedIntoView = useEventCallback(() => {
    const { navbarMeta, navbarChildMeta } = getMeta(navbarRef, navbarListRef, theme.direction, value, valueToIndex);
    if (!navbarChildMeta || !navbarMeta) {
      return;
    }
    const scrollOffset = 
      (navbarChildMeta[start] < navbarMeta[start]) ?
      // left side of button is out of view
      navbarMeta[scrollStart] + (navbarChildMeta[start] - navbarMeta[start]) :
      // right side of button is out of view
      navbarMeta[scrollStart] + (navbarChildMeta[end] - navbarMeta[end]);
    scroll(scrollStart, navbarRef, scrollOffset);
  });


  React.useEffect(() => {
    const handleResize = _.debounce(() => {
      updateIndicatorState();
      updateScrollButtonState();
    });

    const win = ownerWindow(navbarRef.current);
    win.addEventListener('resize', handleResize);
    return () => {
      handleResize.cancel();
      win.removeEventListener('resize', handleResize);
    };
  }, [updateIndicatorState, updateScrollButtonState, navbarRef]);

  React.useEffect(() => {
    updateIndicatorState();
    updateScrollButtonState();
  });

  React.useEffect(() => {
    scrollSelectedIntoView();
  }, [scrollSelectedIntoView, indicatorStyle]);

  // React.useImperativeHandle(
  //   action,
  //   () => ({
  //     updateIndicator: updateIndicatorState,
  //     updateScrollButtons: updateScrollButtonState,
  //   }),
  //   [updateIndicatorState, updateScrollButtonState],
  // );

  return <React.Fragment>
          {showScrollButtons ? (
            <ScrollButtonComponent
              orientation={orientation}
              direction={isRtl ? 'right' : 'left'}
              onClick={handleStartScrollClick}
              disabled={!displayScroll.start}
              className={clsx(classes.scrollButtons, {
                [classes.scrollButtonsDesktop]: scrollButtons !== 'on',
              })}
              {...NavbarScrollButtonProps}
            />
          ) : null}
          <ScrollbarSize className={classes.scrollable} onChange={handleScrollbarSizeChange} />
          <div
            className={clsx(classes.scroller, {
              [classes.fixed]: !scrollable,
              [classes.scrollable]: scrollable,
            })}
            style={scrollerStyle}
            ref={navbarRef}
            onScroll={handleNavbarScroll}>
            <NavbarList ariaLabel={ariaLabel} 
                        ariaLabelledBy={ariaLabelledBy} 
                        classes={classes} 
                        vertical={vertical} 
                        isCentered={isCentered} 
                        isScrollable={scrollable}
                        isRtl={isRtl}
                        ref={navbarListRef}
                        {...other}>
              {children}
            </NavbarList>
            {mounted && indicator}
          </div>
          {showScrollButtons ? (
            <ScrollButtonComponent
              orientation={orientation}
              direction={isRtl ? 'left' : 'right'}
              onClick={handleEndScrollClick}
              disabled={!displayScroll.end}
              className={clsx(classes.scrollButtons, {
                [classes.scrollButtonsDesktop]: scrollButtons !== 'on',
              })}
              {...NavbarScrollButtonProps}
            />
          ) : null}  
        </React.Fragment>
})



const NavbarList = React.forwardRef(function NavbarList(props, ref) {
  const {
    ariaLabel,
    ariaLabelledBy,
    classes,
    vertical,
    isCentered,
    isRtl,
    isScrollable,
    children
  } = props

  const handleKeyDown = (event) => {
    const { target } = event;
    // Keyboard navigation assumes that [role="link"] are siblings
    // though we might warn in the future about nested, interactive elements
    // as a a11y violation
    const role = target.getAttribute('rolw');
    if (role !== 'link') {
      return;
    }

    let newFocusTarget = null;
    let previousItemKey = vertical ? 'ArrowUp' : 'ArrowLeft';
    let nextItemKey = vertical ? 'ArrowDown' : 'ArrowRight';
    if (!vertical && isRtl) {
      // swap previousItemKey with nextItemKey
      previousItemKey = 'ArrowRight';
      nextItemKey = 'ArrowLeft';
    }

    switch (event.key) {
      case previousItemKey:
        newFocusTarget = target.previousElementSibling || ref.current.lastChild;
        break;
      case nextItemKey:
        newFocusTarget = target.nextElementSibling || ref.current.firstChild;
        break;
      case 'Home':
        newFocusTarget = ref.current.firstChild;
        break;
      case 'End':
        newFocusTarget = ref.current.lastChild;
        break;
      default:
        break;
    }

    if (newFocusTarget !== null) {
      newFocusTarget.focus();
      event.preventDefault();
    }
  };

  return <div
            aria-label={ariaLabel}
            aria-labelledby={ariaLabelledBy}
            className={clsx(classes.flexContainer, {
              [classes.flexContainerVertical]: vertical,
              [classes.centered]: isCentered && !isScrollable,
            })}
            onKeyDown={handleKeyDown}
            ref={ref}>
              {children}
        </div>
})


const getNavbarMeta = (navbarRef, direction)=> {

  const navbarNode = navbarRef.current;
  let navbarMeta =null
  if (navbarNode) {
    const rect = navbarNode.getBoundingClientRect();
    // create a new object with ClientRect class props + scrollLeft
    navbarMeta = {
      clientWidth: navbarNode.clientWidth,
      scrollLeft: navbarNode.scrollLeft,
      scrollTop: navbarNode.scrollTop,
      scrollLeftNormalized: getNormalizedScrollLeft(navbarNode, direction),
      scrollWidth: navbarNode.scrollWidth,
      top: rect.top,
      bottom: rect.bottom,
      left: rect.left,
      right: rect.right,
    };
  }
  return navbarMeta
}

const getMeta = (navbarRef, navbarListRef, direction, value, valueToIndex) => {
  let navbarMeta=getNavbarMeta(navbarRef, direction)
  let navbarChild=null
  let navbarChildMeta=null
  if (navbarMeta && value) {
    const children = navbarListRef.current.children;
    if (children.length > 0) {
      const child = children[valueToIndex.get(value)];
      if (process.env.NODE_ENV !== 'production' && !child) {
        console.error(
            `Material-UI: The value provided to the Navbar component is invalid. 
            None of the Navbar's children match with \`${value}\`. You can provided
            one of the following values - ${_.join(valueToIndex.keys, ', ')}`);
      }
      navbarChildMeta = child ? child.getBoundingClientRect() : null;
    }
  }
  return { navbarMeta, navbarChild, navbarChildMeta };
}


function computeIndicatorStyle (navbarRef, navbarListRef, vertical, direction, value, valueToIndex) {
  const { navbarMeta, navbarChildMeta } = getMeta(navbarRef, navbarListRef, direction, value, valueToIndex)

  const start = vertical ? 'top' : 'left';
  const end = vertical ? 'bottom' : 'right';
  const clientSize = vertical ? 'clientHeight' : 'clientWidth';
  const size = vertical ? 'height' : 'width';
  let startValue = 0;

  if (navbarChildMeta && navbarMeta) {
    if (vertical) {
      startValue = navbarChildMeta.top - navbarMeta.top + navbarMeta.scrollTop;
    } else {
      const correction = direction === 'rtl'
        ? navbarMeta.scrollLeftNormalized + navbarMeta.clientWidth - navbarMeta.scrollWidth
        : navbarMeta.scrollLeft;
      startValue = navbarChildMeta.left - navbarMeta.left + correction;
    }
  }

  return {
    [start]: startValue,
    // May be wrong until the font is loaded.
    [size]: navbarChildMeta ? navbarChildMeta[size] : 0,
  };
}



Navbar.propTypes = {
   /**
   * The label for the Tabs as a string.
   */
  'aria-label': PropTypes.string,
  /**
   * An id or list of ids separated by a space that label the Tabs.
   */
  'aria-labelledby': PropTypes.string,
  /**
   * If `true`, the tabs will be centered.
   * This property is intended for large views.
   */
  isCentered: PropTypes.bool,
  /**
   * The content of the component.
   */
  children: PropTypes.node,
  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css) below for more details.
   */
  classes: PropTypes.object,
  /**
   * @ignore
   */
  className: PropTypes.string,
  /**
   * The component used for the root node.
   * Either a string to use a HTML element or a component.
   */
  component: PropTypes.elementType,
  /**
   * Determines the color of the indicator.
   */
  indicatorColor: PropTypes.oneOf(['primary', 'secondary']),
  
  /**
   * The tabs orientation (layout flow direction).
   */
  orientation: PropTypes.oneOf(['horizontal', 'vertical']),
  /**
   * The component used to render the scroll buttons.
   */
  ScrollButtonComponent: PropTypes.elementType,
  /**
   * Determine behavior of scroll buttons when tabs are set to scroll:
   *
   * - `auto` will only present them when not all the items are visible.
   * - `desktop` will only present them on medium and larger viewports.
   * - `on` will always present them.
   * - `off` will never present them.
   */
  scrollButtons: PropTypes.oneOf(['auto', 'desktop', 'off', 'on']),
  /**
   * Props applied to the Navbar indicator element.
   */
  NavbarIndicatorProps: PropTypes.object,
  /**
   * Props applied to the NavbarScrollButton element.
   */
  NavbarScrollButtonProps: PropTypes.object,
  /**
   * Determines the color of the `Navbar`.
   */
  textColor: PropTypes.oneOf(['inherit', 'primary', 'secondary']),
  /**
   * The value of the currently selected `Navbar`.
   * If you don't want any selected `Navbar`, you can set this property to `false`.
   */
  value: PropTypes.any,

  /**
   * Should navigation to the set value be triggered. It will be true when user clicks on a link in navbar. 
   * This should be set to false when the value is changed due to actions such as manual user scroll.
   */
  triggerNavigation : PropTypes.bool,
  /**
   * Determines additional display behavior of the tabs:
   *
   *  - `scrollable` will invoke scrolling properties and allow for horizontally
   *  scrolling (or swiping) of the Navbar.
   *  -`fullWidth` will make the tabs grow to use all the available space,
   *  which should be used for small views, like on mobile.
   *  - `standard` will render the default state.
   */
  variant: PropTypes.oneOf(['fullWidth', 'scrollable', 'standard']),

  /**
   * The offset from the target position to scroll to.
   */
  offsetToTarget: PropTypes.number
};




export default withStyles(styles, { name: 'EnsembleNavbar' })(Navbar);

